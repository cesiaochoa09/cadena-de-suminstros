from django import forms
from core.models import *

class TipoForm(forms.ModelForm):
    class Meta:
        model = Tipo
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={"type":"text","class":"form-control"}),
        }

class PuestoForm(forms.ModelForm):
    class Meta:
        model = Puesto
        fields = "__all__"
        widgets = {
            "nombre": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "ubicacion": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
        }

class DistribuidorForm(forms.ModelForm):
    class Meta:
        model = Distribuidor
        fields = "__all__"
        widgets = {
            "numero": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "dir_calle": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "dir_numero": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "dir_colonia": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "dir_cp": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "cantacto_nombre": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "cantacto_pellido_paterno": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "cantacto_apellido_materno": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "cantacto_numero_telefono": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
        }

class ProveedorForm(forms.ModelForm):
    class Meta:
        model = Proveedor
        fields = "__all__"
        widgets = {
            "numero": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "dir_calle": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "dir_numero": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "dir_colonia": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "dir_cp": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "cantacto_nombre": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "cantacto_pellido_paterno": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "cantacto_apellido_materno": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "cantacto_numero_telefono": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
        }

class CategoriaForm(forms.ModelForm):
    class Meta:
        model = Categoria
        fields = "__all__"
        widgets = {
            "numero": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "descripcion": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
        }

class EstadoPedidoForm(forms.ModelForm):
    class Meta:
        model = EstadoPedido
        fields = "__all__"
        widgets = {
            "numero": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "descripcion": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
        }  

class EstadoTransporteForm(forms.ModelForm):
    class Meta:
        model = EstadoTransporte
        fields = "__all__"
        widgets = {
            "numero": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "descripcion": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
        }  

class DepartamentoForm(forms.ModelForm):
       class Meta:
        model = Departamento
        fields = "__all__"
        widgets = {
            "numero": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "descripcion": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "puesto": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
        }  

class EmpleadoForm(forms.ModelForm):
       class Meta:
        model = Empleado
        fields = "__all__"
        widgets = {
            "numero": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "descripcion": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "apellido_paterno": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "apellido_materno": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "puesto": forms.Select(attrs={"type":"text", "class":"form-control"}),
        } 

class TransporteForm(forms.ModelForm):
       class Meta:
        model = Transporte
        fields = "__all__"
        widgets = {
            "numero": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "descripcion": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "empleado": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "tipo": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "estado": forms.Select(attrs={"type":"select", "class":"form-control"}),
        } 

class PlantaForm(forms.ModelForm):
       class Meta:
        model = Planta
        fields = "__all__"
        widgets = {
            "numero": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "dir_calle": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "dir_numero": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "dir_colonia": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "dir_cp": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "numero_telefono": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "proveedor": forms.Select(attrs={"type":"text", "class":"form-control"}),
            "empleado": forms.Select(attrs={"type":"text", "class":"form-control"}),
        } 

class ProductoForm(forms.ModelForm):
       class Meta:
        model = Producto
        fields = "__all__"
        widgets = {
            "numero": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "descripcion": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "precio": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "proveedor": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "distribuidor": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "categoria": forms.Select(attrs={"type":"select", "class":"form-control"}),
        } 

class PedidoForm(forms.ModelForm):
       class Meta:
        model = Pedido
        fields = "__all__"
        widgets = {
            "numero": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "fecha": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "cantidad": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "precio_total": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "planta": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "producto": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "transporte": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "estado": forms.Select(attrs={"type":"select", "class":"form-control"}),
        }  

class AlmacenForm(forms.ModelForm):
       class Meta:
        model = Almacen
        fields = "__all__"
        widgets = {
            "numero": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "fecha": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "responsable": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "apellido_paterno": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "apellido_materno": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "producto": forms.Select(attrs={"type":"select", "class":"form-control"}),
        }  

class FacturaForm(forms.ModelForm):
       class Meta:
        model = Factura
        fields = "__all__"
        widgets = {
            "numero": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "fecha_facturacion": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "importe": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "iva": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "toal": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "pedido": forms.Select(attrs={"type":"select", "class":"form-control"}),
        }   

class AlmacenProductoForm(forms.ModelForm):
       class Meta:
        model = AlmacenProducto
        fields = "__all__"
        widgets = {
            "almacen": forms.Select(attrs={"type":"select","class":"form-control"}),
            "producto": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "stock": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
        }    

class PedidoProductoForm(forms.ModelForm):
       class Meta:
        model = PedidoProducto
        fields = "__all__"
        widgets = {
            "pedido": forms.TextInput(attrs={"type":"text","class":"form-control"}),
            "producto": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "importe": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "cantidad": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
        }             