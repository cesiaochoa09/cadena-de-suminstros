from django.db import models

# Create your models here.
class Puesto(models.Model):
    numero = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    ubicacion = models.CharField(max_length=20)

class Tipo(models.Model):
    numero = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=60)

class Distribuidor(models.Model):
    numero = models.AutoField(primary_key=True)
    dir_calle = models.CharField(max_length=30)
    dir_numero = models.CharField(max_length=10)
    dir_colonia = models.CharField(max_length=30)
    dir_cp = models.CharField(max_length=5)
    nombre = models.CharField(max_length=30)
    cantacto_nombre = models.CharField(max_length=30)
    contacto_apellido_paterno = models.CharField(max_length=30)
    contacto_apellido_materno = models.CharField(max_length=30)
    caontacto_numero_telefono = models.CharField(max_length=15)

class Proveedor(models.Model):
    numero = models.AutoField(primary_key=True)
    dir_calle = models.CharField(max_length=30)
    dir_numero = models.CharField(max_length=10)
    dir_colonia = models.CharField(max_length=30)
    dir_cp = models.CharField(max_length=5)
    nombre = models.CharField(max_length=30)
    contacto_nombre = models.CharField(max_length=30)
    contacto_apellido_paterno = models.CharField(max_length=30)
    contacto_apellido_materno = models.CharField(max_length=30)
    contacto_numero_telefono = models.CharField(max_length=15)

    def __str__(self):
        return self.nombre
    

class Categoria(models.Model):
    numero = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=60)

class EstadoPedido(models.Model):
    numero = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=60)

class EstadoTransporte(models.Model):
    numero = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=60)

class Departamento(models.Model):
    numero = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=60)
    descripcion = models.CharField(max_length=60)
    puesto = models.ForeignKey('Puesto', on_delete=models.CASCADE)

class Empleado(models.Model):
    numero = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=60)
    descripcion = models.CharField(max_length=60)
    nombre = models.CharField(max_length=30)
    apellido_paterno = models.CharField(max_length=30)
    apellido_materno = models.CharField(max_length=30)
    puesto = models.ForeignKey('Puesto', on_delete=models.CASCADE)

class Transporte(models.Model):
    numero = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=60)
    empleado = models.ForeignKey('Empleado', on_delete=models.CASCADE)
    tipo = models.ForeignKey('Tipo', on_delete=models.CASCADE)
    estado = models.ForeignKey('EstadoTransporte', on_delete=models.CASCADE)  # Corregido el nombre del modelo


class Planta(models.Model):
    numero = models.AutoField(primary_key=True)
    dir_calle = models.CharField(max_length=30)
    dir_numero = models.CharField(max_length=10)
    dir_colonia = models.CharField(max_length=30)
    dir_cp = models.CharField(max_length=5)
    numero_telefono = models.CharField(max_length=15)
    proveedor = models.ForeignKey('Proveedor', on_delete=models.CASCADE)
    empleado = models.ForeignKey('Empleado', on_delete=models.CASCADE)

class Producto(models.Model):
    numero = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=60)
    precio = models.FloatField()
    proveedor = models.ForeignKey('Proveedor', on_delete=models.CASCADE)
    distribuidor = models.ForeignKey('Distribuidor', on_delete=models.CASCADE)
    categoria = models.ForeignKey('Categoria', on_delete=models.CASCADE)

class Pedido(models.Model):
    numero = models.AutoField(primary_key=True)
    fecha = models.DateField(auto_now_add=False, auto_now=False, blank=True, null=True)
    cantidad = models.IntegerField()
    precio_total = models.FloatField()
    planta = models.ForeignKey('Planta', on_delete=models.CASCADE)
    producto = models.ForeignKey('Producto', on_delete=models.CASCADE)
    distribuidor = models.ForeignKey('Distribuidor', on_delete=models.CASCADE)
    transporte = models.ForeignKey('Transporte', on_delete=models.CASCADE)
    estado = models.ForeignKey('EstadoPedido', on_delete=models.CASCADE)

    

class Almacen(models.Model):
    numero = models.AutoField(primary_key=True)
    fecha = models.DateField(auto_now_add=False, auto_now=False, blank=True, null=True)
    responsable = models.CharField(max_length=30)
    nombre = models.CharField(max_length=30)
    apellido_paterno = models.CharField(max_length=30)
    apellido_materno = models.CharField(max_length=30)
    producto = models.ForeignKey('Producto', on_delete=models.CASCADE)


class Factura(models.Model):
    numero = models.AutoField(primary_key=True)
    fecha_facturacion = models.DateField(auto_now_add=False, auto_now=False, blank=True, null=False)
    importe = models.FloatField()
    iva = models.FloatField()
    total = models.FloatField()
    pedido = models.ForeignKey('Pedido', on_delete=models.CASCADE)

class AlmacenProducto(models.Model):
    almacen = models.ForeignKey('Almacen', on_delete=models.CASCADE)
    producto = models.ForeignKey('Producto', on_delete=models.CASCADE)
    stock = models.IntegerField()

class PedidoProducto(models.Model):
    pedido = models.ForeignKey('Pedido', on_delete=models.CASCADE)
    producto = models.ForeignKey('Producto', on_delete=models.CASCADE)
    importe = models.FloatField()
    cantidad = models.IntegerField()

