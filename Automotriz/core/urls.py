from django.urls import path
from core import views

app_name = 'core'

urlpatterns = [
    # Tipo
    path('tipo/list/', views.ListTipo.as_view(), name="tipo_list"),
    path('tipo/create/', views.CreateTipo.as_view(), name="tipo_create"),
    path('tipo/detail/<int:pk>/', views.DetailTipo.as_view(), name="tipo_detail"),
    path('tipo/update/<int:pk>/', views.UpdateTipo.as_view(), name="tipo_update"),
    path('tipo/delete/<int:pk>/', views.DeleteTipo.as_view(), name="tipo_delete"),
    
    # Puestos
    path('puesto/list/', views.ListPuesto.as_view(), name="puesto_list"),
    path('puesto/create/', views.CreatePuesto.as_view(), name="puesto_create"),
    path('puesto/detail/<int:pk>/', views.DetailPuesto.as_view(), name="puesto_detail"),
    path('puesto/update/<int:pk>/', views.UpdatePuesto.as_view(), name="puesto_update"),
    path('puesto/delete/<int:pk>/', views.DeletePuesto.as_view(), name="puesto_delete"),


    # Distribuidor
    path('distribuidor/list/', views.ListDistribuidor.as_view(), name="distribuidor_list"),
    path('distribuidor/create/', views.CreateDistribuidor.as_view(), name="distribuidor_create"),
    path('distribuidor/detail/<int:pk>/', views.DetailDistribuidor.as_view(), name="distribuidor_detail"),
    path('distribuidor/update/<int:pk>/', views.UpdateDistribuidor.as_view(), name="distribuidor_update"),
    path('distribuidor/delete/<int:pk>/', views.DeleteDistribuidor.as_view(), name="distribuidor_delete"),

    # Proveedor
    path('proveedor/list/', views.ListProveedor.as_view(), name="proveedor_list"),
    path('proveedor/create/', views.CreateProveedor.as_view(), name="proveedor_create"),
    path('proveedor/detail/<int:pk>/', views.DetailProveedor.as_view(), name="proveedor_detail"),
    path('proveedor/update/<int:pk>/', views.UpdateProveedor.as_view(), name="proveedor_update"),
    path('proveedor/delete/<int:pk>/', views.DeleteProveedor.as_view(), name="proveedor_delete"),

    # Categoria
    path('categoria/list/', views.ListCategoria.as_view(), name="categoria_list"),
    path('categoria/create/', views.CreateCategoria.as_view(), name="categoria_create"),
    path('categoria/detail/<int:pk>/', views.DetailCategoria.as_view(), name="categoria_detail"),
    path('categoria/update/<int:pk>/', views.UpdateCategoria.as_view(), name="categoria_update"),
    path('categoria/delete/<int:pk>/', views.DeleteCategoria.as_view(), name="categoria_delete"),

    # EstadoPedido
    path('estadopedido/list/', views.ListEstadoPedido.as_view(), name="estadopedido_list"),
    path('estadopedido/create/', views.CreateEstadoPedido.as_view(), name="estadopedido_create"),
    path('estadopedido/detail/<int:pk>/', views.DetailEstadoPedido.as_view(), name="estadopedido_detail"),
    path('estadopedido/update/<int:pk>/', views.UpdateEstadoPedido.as_view(), name="estadopedido_update"),
    path('estadopedido/delete/<int:pk>/', views.DeleteEstadoPedido.as_view(), name="estadopedido_delete"),
    
    # EstadoTransporte
    path('estadotransporte/list/', views.ListEstadoTransporte.as_view(), name="estadotransporte_list"),
    path('estadotransporte/create/', views.CreateEstadoTransporte.as_view(), name="estadotransporte_create"),
    path('estadotransporte/detail/<int:pk>/', views.DetailEstadoTransporte.as_view(), name="estadotransporte_detail"),
    path('estadotransporte/update/<int:pk>/', views.UpdateEstadoTransporte.as_view(), name="estadotransporte_update"),
    path('estadotransporte/delete/<int:pk>/', views.DeleteEstadoTransporte.as_view(), name="estadotransporte_delete"),
  
    # Departamento
    path('departamento/list/', views.ListDepartamento.as_view(), name="departamento_list"),
    path('departamento/create/', views.CreateDepartamento.as_view(), name="departamento_create"),
    path('departamento/detail/<int:pk>/', views.DetailDepartamento.as_view(), name="departamento_detail"),
    path('departamento/update/<int:pk>/', views.UpdateDepartamento.as_view(), name="departamento_update"),
    path('departamento/delete/<int:pk>/', views.DeleteDepartamento.as_view(), name="departamento_delete"),
   
    # Empleado
    path('empleado/list/', views.ListEmpleado.as_view(), name="empleado_list"),
    path('empleado/create/', views.CreateEmpleado.as_view(), name="empleado_create"),
    path('empleado/detail/<int:pk>/', views.DetailEmpleado.as_view(), name="empleado_detail"),
    path('empleado/update/<int:pk>/', views.UpdateEmpleado.as_view(), name="empleado_update"),
    path('empleado/delete/<int:pk>/', views.DeleteEmpleado.as_view(), name="empleado_delete"),
   
     # Transporte
    path('transporte/list/', views.ListTransporte.as_view(), name="transporte_list"),
    path('transporte/create/', views.CreateTransporte.as_view(), name="transporte_create"),
    path('transporte/detail/<int:pk>/', views.DetailTransporte.as_view(), name="transporte_detail"),
    path('transporte/update/<int:pk>/', views.UpdateTransporte.as_view(), name="transporte_update"),
    path('transporte/delete/<int:pk>/', views.DeleteTransporte.as_view(), name="transporte_delete"),
   
    # Planta
    path('planta/list/', views.ListPlanta.as_view(), name="planta_list"),
    path('planta/create/', views.CreatePlanta.as_view(), name="planta_create"),
    path('planta/detail/<int:pk>/', views.DetailPlanta.as_view(), name="planta_detail"),
    path('planta/update/<int:pk>/', views.UpdatePlanta.as_view(), name="planta_update"),
    path('planta/delete/<int:pk>/', views.DeletePlanta.as_view(), name="planta_delete"),
   
    # Producto
    path('producto/list/', views.ListProducto.as_view(), name="producto_list"),
    path('producto/create/', views.CreateProducto.as_view(), name="producto_create"),
    path('producto/detail/<int:pk>/', views.DetailProducto.as_view(), name="producto_detail"),
    path('producto/update/<int:pk>/', views.UpdateProducto.as_view(), name="producto_update"),
    path('producto/delete/<int:pk>/', views.DeleteProducto.as_view(), name="producto_delete"),
    
    # Pedido
    path('pedido/list/', views.ListPedido.as_view(), name="pedido_list"),
    path('pedido/create/', views.CreatePedido.as_view(), name="pedido_create"),
    path('pedido/detail/<int:pk>/', views.DetailPedido.as_view(), name="pedido_detail"),
    path('pedido/update/<int:pk>/', views.UpdatePedido.as_view(), name="pedido_update"),
    path('pedido/delete/<int:pk>/', views.DeletePedido.as_view(), name="pedido_delete"),

    # Almacen
    path('almacen/list/', views.ListAlmacen.as_view(), name="almacen_list"),
    path('almacen/create/', views.CreateAlmacen.as_view(), name="almacen_create"),
    path('almacen/detail/<int:pk>/', views.DetailAlmacen.as_view(), name="almacen_detail"),
    path('almacen/update/<int:pk>/', views.UpdateAlmacen.as_view(), name="almacen_update"),
    path('almacen/delete/<int:pk>/', views.DeleteAlmacen.as_view(), name="almacen_delete"),

    # Factura
    path('factura/list/', views.ListFactura.as_view(), name="factura_list"),
    path('factura/create/', views.CreateFactura.as_view(), name="factura_create"),
    path('factura/detail/<int:pk>/', views.DetailFactura.as_view(), name="factura_detail"),
    path('factura/update/<int:pk>/', views.UpdateFactura.as_view(), name="factura_update"),
    path('factura/delete/<int:pk>/', views.DeleteFactura.as_view(), name="factura_delete"),

    # AlmacenProducto
    path('almacenproducto/list/', views.ListAlmacenProducto.as_view(), name="almacenproducto_list"),
    path('almacenproducto/create/', views.CreateAlmacenProducto.as_view(), name="almacenproducto_create"),
    path('almacenproducto/detail/<int:pk>/', views.DetailAlmacenProducto.as_view(), name="almacenproducto_detail"),
    path('almacenproducto/update/<int:pk>/', views.UpdateAlmacenProducto.as_view(), name="almacenproducto_update"),
    path('almacenproducto/delete/<int:pk>/', views.DeleteAlmacenProducto.as_view(), name="almacenproducto_delete"),

    # PedidoProducto
    path('pedidoproducto/list/', views.ListPedidoProducto.as_view(), name="pedidoproducto_list"),
    path('pedidoproducto/create/', views.CreatePedidoProducto.as_view(), name="pedidoproducto_create"),
    path('pedidoproducto/detail/<int:pk>/', views.DetailPedidoProducto.as_view(), name="pedidoproducto_detail"),
    path('pedidoproducto/update/<int:pk>/', views.UpdatePedidoProducto.as_view(), name="pedidoproducto_update"),
    path('pedidoproducto/delete/<int:pk>/', views.DeletePedidoProducto.as_view(), name="pedidoproducto_delete"),

    ]