from django.shortcuts import render

from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin

from django.urls import reverse_lazy
from .models import *
from .forms import *

# Create your views here.

# ========================= Tipo ============================================
class CreateTipo(generic.CreateView):
    template_name = "core/tipo/tipo_create.html"
    model = Tipo
    form_class = TipoForm
    success_url = reverse_lazy("core:tipo_list")
    login_url = reverse_lazy("home:index")

#List 
class ListTipo(generic.View):
    template_name = "core/tipo/tipo_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = Tipo.objects.all()
        self.context = {
            "tipo": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailTipo(generic.DetailView):
    template_name = "core/tipo/tipo_detail.html"
    model = Tipo
    login_url = reverse_lazy("home:index")


#Update
class UpdateTipo(generic.UpdateView):
    template_name = "core/tipo/tipo_update.html"
    model = Tipo
    form_class = TipoForm
    success_url = reverse_lazy("core:tipo_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeleteTipo(generic.DeleteView):
    template_name = "core/tipo/tipo_delete.html"
    model = Tipo
    success_url = reverse_lazy("core:gg_list")
    login_url = reverse_lazy("home:index")


# ========================= Puesto ============================================
# Create
class CreatePuesto(generic.CreateView):
    template_name = "core/puestos/puesto_create.html"
    model = Puesto
    form_class = PuestoForm
    success_url = reverse_lazy("core:puesto_list")
    login_url = reverse_lazy("home:index")

#List 
class ListPuesto(generic.View):
    template_name = "core/puestos/puesto_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = Puesto.objects.all()
        self.context = {
            "puesto": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailPuesto(generic.DetailView):
    template_name = "core/puestos/puesto_detail.html"
    model = Puesto
    login_url = reverse_lazy("home:index")

#Update
class UpdatePuesto(generic.UpdateView):
    template_name = "core/puestos/puesto_update.html"
    model = Puesto
    form_class = PuestoForm
    success_url = reverse_lazy("core:puesto_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeletePuesto(generic.DeleteView):
    template_name = "core/puestos/puesto_delete.html"
    model = Puesto
    success_url = reverse_lazy("core:puesto_list")
    login_url = reverse_lazy("home:index")


    # ========================= Distribuidor ============================================
# Create
class CreateDistribuidor(generic.CreateView):
    template_name = "core/distribuidor/distribuidor_create.html"
    model = Distribuidor
    form_class = DistribuidorForm
    success_url = reverse_lazy("core:distribuidor_list")
    login_url = reverse_lazy("home:index")

#List 
class ListDistribuidor(generic.View):
    template_name = "core/distribuidor/distribuidor_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = Distribuidor.objects.all()
        self.context = {
            "distribuidor": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailDistribuidor(generic.DetailView):
    template_name = "core/distribuidor/distribuidor_detail.html"
    model = Distribuidor
    login_url = reverse_lazy("home:index")

#Update
class UpdateDistribuidor(generic.UpdateView):
    template_name = "core/distribuidor/distribuidor_update.html"
    model = Distribuidor
    form_class = DistribuidorForm
    success_url = reverse_lazy("core:distribuidor_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeleteDistribuidor(generic.DeleteView):
    template_name = "core/distribuidor/distribuidor_delete.html"
    model = Distribuidor
    success_url = reverse_lazy("core:distribuidor_list")
    login_url = reverse_lazy("home:index")


# ========================= Proveedor ============================================
# Create
class CreateProveedor(generic.CreateView):
    template_name = "core/proveedor/proveedor_create.html"
    model = Proveedor
    form_class = ProveedorForm
    success_url = reverse_lazy("core:proveedor_list")
    login_url = reverse_lazy("home:index")

#List 
class ListProveedor(generic.View):
    template_name = "core/proveedor/proveedor_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = Proveedor.objects.all()
        self.context = {
            "proveedor": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailProveedor(generic.DetailView):
    template_name = "core/proveedor/proveedor_detail.html"
    model = Proveedor
    login_url = reverse_lazy("home:index")

#Update
class UpdateProveedor(generic.UpdateView):
    template_name = "core/proveedor/proveedor_update.html"
    model = Proveedor
    form_class = ProveedorForm
    success_url = reverse_lazy("core:proveedor_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeleteProveedor(generic.DeleteView):
    template_name = "core/proveedor/proveedor_delete.html"
    model = Proveedor
    success_url = reverse_lazy("core:proveedor_list")
    login_url = reverse_lazy("home:index")

    # ========================= Categoria ============================================
# Create
class CreateCategoria(generic.CreateView):
    template_name = "core/categoria/categoria_create.html"
    model = Categoria
    form_class = CategoriaForm
    success_url = reverse_lazy("core:categoria_list")
    login_url = reverse_lazy("home:index")

#List 
class ListCategoria(generic.View):
    template_name = "core/categoria/categoria_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = Categoria.objects.all()
        self.context = {
            "categoria": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailCategoria(generic.DetailView):
    template_name = "core/categoria/categoria_detail.html"
    model = Categoria
    login_url = reverse_lazy("home:index")

#Update
class UpdateCategoria(generic.UpdateView):
    template_name = "core/categoria/categoria_update.html"
    model = Categoria
    form_class = CategoriaForm
    success_url = reverse_lazy("core:categoria_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeleteCategoria(generic.DeleteView):
    template_name = "core/categoria/categoria_delete.html"
    model = Categoria
    success_url = reverse_lazy("core:categoria_list")
    login_url = reverse_lazy("home:index")

     # ========================= EstadoPedido ============================================
# Create
class CreateEstadoPedido(generic.CreateView):
    template_name = "core/estadopedido/estadopedido_create.html"
    model = EstadoPedido
    form_class = EstadoPedidoForm
    success_url = reverse_lazy("core:estadopedido_list")
    login_url = reverse_lazy("home:index")

#List 
class ListEstadoPedido(generic.View):
    template_name = "core/estadopedido/estadopedido_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = EstadoPedido.objects.all()
        self.context = {
            "estadopedido": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailEstadoPedido(generic.DetailView):
    template_name = "core/estadopedido/estadopedido_detail.html"
    model = EstadoPedido
    login_url = reverse_lazy("home:index")

#Update
class UpdateEstadoPedido(generic.UpdateView):
    template_name = "core/estadopedido/estadopedido_update.html"
    model = EstadoPedido
    form_class = EstadoPedidoForm
    success_url = reverse_lazy("core:estadopedido_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeleteEstadoPedido(generic.DeleteView):
    template_name = "core/estadopedido/estadopedido_delete.html"
    model = EstadoPedido
    success_url = reverse_lazy("core:estadopedido_list")
    login_url = reverse_lazy("home:index")

  # ========================= EstadoTransporte ============================================
# Create
class CreateEstadoTransporte(generic.CreateView):
    template_name = "core/estadotransporte/estadotransporte_create.html"
    model = EstadoTransporte
    form_class = EstadoTransporteForm
    success_url = reverse_lazy("core:estadotransporte_list")
    login_url = reverse_lazy("home:index")

#List 
class ListEstadoTransporte(generic.View):
    template_name = "core/estadotransporte/estadotransporte_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = EstadoTransporte.objects.all()
        self.context = {
            "estadotransporte": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailEstadoTransporte(generic.DetailView):
    template_name = "core/estadotransporte/estadotransporte_detail.html"
    model = EstadoTransporte
    login_url = reverse_lazy("home:index")

#Update
class UpdateEstadoTransporte(generic.UpdateView):
    template_name = "core/estadotransporte/estadotransporte_update.html"
    model = EstadoTransporte
    form_class = EstadoTransporteForm
    success_url = reverse_lazy("core:estadotransporte_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeleteEstadoTransporte(generic.DeleteView):
    template_name = "core/estadotransporte/estadotransporte_delete.html"
    model = EstadoTransporte
    success_url = reverse_lazy("core:estadotransporte_list")
    login_url = reverse_lazy("home:index") 

# ========================= Departamento ============================================
# Create
class CreateDepartamento(generic.CreateView):
    template_name = "core/departamento/departamento_create.html"
    model = Departamento
    form_class = DepartamentoForm
    success_url = reverse_lazy("core:departamento_list")
    login_url = reverse_lazy("home:index")

#List 
class ListDepartamento(generic.View):
    template_name = "core/departamento/departamento_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = Departamento.objects.all()
        self.context = {
            "departamento": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailDepartamento(generic.DetailView):
    template_name = "core/departamento/departamento_detail.html"
    model = Departamento
    login_url = reverse_lazy("home:index")

#Update
class UpdateDepartamento(generic.UpdateView):
    template_name = "core/departamento/departamento_update.html"
    model = Departamento
    form_class = DepartamentoForm
    success_url = reverse_lazy("core:departamento_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeleteDepartamento(generic.DeleteView):
    template_name = "core/departamento/departamento_delete.html"
    model = Departamento
    success_url = reverse_lazy("core:departamento_list")
    login_url = reverse_lazy("home:index")   

  # ========================= Empleado ============================================
# Create
class CreateEmpleado(generic.CreateView):
    template_name = "core/empleado/empleado_create.html"
    model = Empleado
    form_class = EmpleadoForm
    success_url = reverse_lazy("core:empleado_list")
    login_url = reverse_lazy("home:index")

#List 
class ListEmpleado(generic.View):
    template_name = "core/empleado/empleado_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = Empleado.objects.all()
        self.context = {
            "empleado": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailEmpleado(generic.DetailView):
    template_name = "core/empleado/empleado_detail.html"
    model = Empleado
    login_url = reverse_lazy("home:index")

#Update
class UpdateEmpleado(generic.UpdateView):
    template_name = "core/empleado/empleado_update.html"
    model = Empleado
    form_class = EmpleadoForm
    success_url = reverse_lazy("core:empleado_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeleteEmpleado(generic.DeleteView):
    template_name = "core/empleado/empleado_delete.html"
    model = Empleado
    success_url = reverse_lazy("core:empleado_list")
    login_url = reverse_lazy("home:index")  

    # ========================= Transporte ============================================
# Create
class CreateTransporte(generic.CreateView):
    template_name = "core/transporte/transporte_create.html"
    model = Transporte
    form_class = TransporteForm
    success_url = reverse_lazy("core:transporte_list")
    login_url = reverse_lazy("home:index")

#List 
class ListTransporte(generic.View):
    template_name = "core/transporte/transporte_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = Transporte.objects.all()
        self.context = {
            "transporte": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailTransporte(generic.DetailView):
    template_name = "core/transporte/transporte_detail.html"
    model = Transporte
    login_url = reverse_lazy("home:index")

#Update
class UpdateTransporte(generic.UpdateView):
    template_name = "core/transporte/transporte_update.html"
    model = Transporte
    form_class = TransporteForm
    success_url = reverse_lazy("core:transporte_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeleteTransporte(generic.DeleteView):
    template_name = "core/transporte/transporte_delete.html"
    model = Transporte
    success_url = reverse_lazy("core:transporte_list")
    login_url = reverse_lazy("home:index") 

    # ========================= Planta ============================================
# Create
class CreatePlanta(generic.CreateView):
    template_name = "core/planta/planta_create.html"
    model = Planta
    form_class = PlantaForm
    success_url = reverse_lazy("core:planta_list")
    login_url = reverse_lazy("home:index")

#List 
class ListPlanta(generic.View):
    template_name = "core/planta/planta_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = Planta.objects.all()
        self.context = {
            "planta": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailPlanta(generic.DetailView):
    template_name = "core/planta/planta_detail.html"
    model = Planta
    login_url = reverse_lazy("home:index")

#Update
class UpdatePlanta(generic.UpdateView):
    template_name = "core/planta/planta_update.html"
    model = Planta
    form_class = PlantaForm
    success_url = reverse_lazy("core:planta_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeletePlanta(generic.DeleteView):
    template_name = "core/planta/planta_delete.html"
    model = Planta
    success_url = reverse_lazy("core:planta_list")
    login_url = reverse_lazy("home:index") 

    # ========================= Producto ============================================
# Create
class CreateProducto(generic.CreateView):
    template_name = "core/producto/producto_create.html"
    model = Producto
    form_class = ProductoForm
    success_url = reverse_lazy("core:producto_list")
    login_url = reverse_lazy("home:index")

#List 
class ListProducto(generic.View):
    template_name = "core/producto/producto_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = Producto.objects.all()
        self.context = {
            "producto": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailProducto(generic.DetailView):
    template_name = "core/producto/producto_detail.html"
    model = Producto
    login_url = reverse_lazy("home:index")

#Update
class UpdateProducto(generic.UpdateView):
    template_name = "core/producto/producto_update.html"
    model = Producto
    form_class = ProductoForm
    success_url = reverse_lazy("core:producto_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeleteProducto(generic.DeleteView):
    template_name = "core/producto/producto_delete.html"
    model = Producto
    success_url = reverse_lazy("core:producto_list")
    login_url = reverse_lazy("home:index") 

    # ========================= Pedido ============================================
# Create
class CreatePedido(generic.CreateView):
    template_name = "core/pedido/pedido_create.html"
    model = Pedido
    form_class = PedidoForm
    success_url = reverse_lazy("core:pedido_list")
    login_url = reverse_lazy("home:index")

#List 
class ListPedido(generic.View):
    template_name = "core/pedido/pedido_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = Pedido.objects.all()
        self.context = {
            "pedido": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailPedido(generic.DetailView):
    template_name = "core/pedido/pedido_detail.html"
    model = Pedido
    login_url = reverse_lazy("home:index")

#Update
class UpdatePedido(generic.UpdateView):
    template_name = "core/pedido/pedido_update.html"
    model = Pedido
    form_class = PedidoForm
    success_url = reverse_lazy("core:pedido_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeletePedido(generic.DeleteView):
    template_name = "core/pedido/pedido_delete.html"
    model = Pedido
    success_url = reverse_lazy("core:pedido_list")
    login_url = reverse_lazy("home:index") 

    # ========================= Almacen ============================================
# Create
class CreateAlmacen(generic.CreateView):
    template_name = "core/almacen/almacen_create.html"
    model = Almacen
    form_class = AlmacenForm
    success_url = reverse_lazy("core:almacen_list")
    login_url = reverse_lazy("home:index")

#List 
class ListAlmacen(generic.View):
    template_name = "core/almacen/almacen_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = Almacen.objects.all()
        self.context = {
            "almacen": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailAlmacen(generic.DetailView):
    template_name = "core/almacen/almacen_detail.html"
    model = Almacen
    login_url = reverse_lazy("home:index")

#Update
class UpdateAlmacen(generic.UpdateView):
    template_name = "core/almacen/almacen_update.html"
    model = Almacen
    form_class = AlmacenForm
    success_url = reverse_lazy("core:almacen_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeleteAlmacen(generic.DeleteView):
    template_name = "core/almacen/almacen_delete.html"
    model = Almacen
    success_url = reverse_lazy("core:almacen_list")
    login_url = reverse_lazy("home:index") 

    # ========================= Factura ============================================
# Create
class CreateFactura(generic.CreateView):
    template_name = "core/factura/factura_create.html"
    model = Factura
    form_class = FacturaForm 
    success_url = reverse_lazy("core:factura_list")
    login_url = reverse_lazy("home:index")

#List 
class ListFactura(generic.View):
    template_name = "core/factura/factura_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = Factura.objects.all()
        self.context = {
            "factura": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailFactura(generic.DetailView):
    template_name = "core/factura/factura_detail.html"
    model = Factura
    login_url = reverse_lazy("home:index")

#Update
class UpdateFactura(generic.UpdateView):
    template_name = "core/factura/factura_update.html"
    model = Factura
    form_class = FacturaForm
    success_url = reverse_lazy("core:factura_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeleteFactura(generic.DeleteView):
    template_name = "core/factura/factura_delete.html"
    model = Factura
    success_url = reverse_lazy("core:factura_list")
    login_url = reverse_lazy("home:index") 

    # ========================= AlmacenProducto ============================================
# Create
class CreateAlmacenProducto(generic.CreateView):
    template_name = "core/almacenproducto/almacenproducto_create.html"
    model = AlmacenProducto
    form_class = AlmacenProductoForm
    success_url = reverse_lazy("core:almacenproducto_list")
    login_url = reverse_lazy("home:index")

#List 
class ListAlmacenProducto(generic.View):
    template_name = "core/almacenproducto/almacenproducto_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = AlmacenProducto.objects.all()
        self.context = {
            "almacenproducto": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailAlmacenProducto(generic.DetailView):
    template_name = "core/almacenproducto/almacenproducto_detail.html"
    model = AlmacenProducto
    login_url = reverse_lazy("home:index")

#Update
class UpdateAlmacenProducto(generic.UpdateView):
    template_name = "core/almacenproducto/almacenproducto_update.html"
    model = AlmacenProducto
    form_class = AlmacenProductoForm
    success_url = reverse_lazy("core:almacenproducto_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeleteAlmacenProducto(generic.DeleteView):
    template_name = "core/almacenproducto/almacenproducto_delete.html"
    model = AlmacenProducto
    success_url = reverse_lazy("core:almacenproducto_list")
    login_url = reverse_lazy("home:index")  

    # ========================= PedidoProducto ============================================
# Create
class CreatePedidoProducto(generic.CreateView):
    template_name = "core/pedidoproducto/pedidoproducto_create.html"
    model = PedidoProducto
    form_class = PedidoProductoForm
    success_url = reverse_lazy("core:pedidoproducto_list")
    login_url = reverse_lazy("home:index")

#List 
class ListPedidoProducto(generic.View):
    template_name = "core/pedidoproducto/pedidoproducto_list.html"
    context = {}
    login_url = reverse_lazy("home:index")

    def get(self, request, *args, **kwargs):
        queryset = PedidoProducto.objects.all()
        self.context = {
            "pedidoproducto": queryset
        }
        return render(request, self.template_name, self.context)

#Detail
class DetailPedidoProducto(generic.DetailView):
    template_name = "core/pedidoproducto/pedidoproducto_detail.html"
    model = PedidoProducto
    login_url = reverse_lazy("home:index")

#Update
class UpdatePedidoProducto(generic.UpdateView):
    template_name = "core/pedidoproducto/pedidoproducto_update.html"
    model = PedidoProducto
    form_class = PedidoProductoForm
    success_url = reverse_lazy("core:pedidoproducto_list")
    login_url = reverse_lazy("home:index")

#Delete
class DeletePedidoProducto(generic.DeleteView):
    template_name = "core/pedidoproducto/pedidoproducto_delete.html"
    model = PedidoProducto
    success_url = reverse_lazy("core:pedidoproducto_list")
    login_url = reverse_lazy("home:index")    