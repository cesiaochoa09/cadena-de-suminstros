from django.shortcuts import render
from django.views import generic
from django.db.models import F, ExpressionWrapper, fields
from django.db.models.functions import Concat, DateFormat
from core.models import *

# Create your views here.
class Index(generic.View):
    template_name = 'home/index.html'
    context = {}

    def get(self, request,*args, **kwargs):
        consulta_django = Pedido.objects.values(
            'num as Numero_de_la_compra',
            ExpressionWrapper(DateFormat('fecha', '%d-%m-%Y'), output_field=fields.CharField())\
                .as_sql(quote_char=None)[0]\
                .replace('"', '')\
                .replace('(', '')\
                .replace(')', '') + ' as Fecha',
            Concat('importe__proveedor__contNombre', ' ', 'importe__proveedor__conAp_Pat', ' ', 'importe__proveedor__conAp_Mat')\
                .as_sql(quote_char=None)[0]\
                .replace('"', '')\
                .replace('(', '')\
                .replace(')', '') + ' as Nombre_contacto_proveedor',
            'precio_total as Monto_total',
            'pedprod__cantidad'
        ).filter(
            pedprod__pedido=F('num'),
            pedprod__producto=F('pedprod__producto'),
            importe__producto=F('pedprod__producto')
        ).select_related(
            'importe__proveedor',
            'pedprod__producto'
        )
        
        self.context = {
            'consulta1': consulta_django,
        }
        
        return render(request, self.template_name, self.context)